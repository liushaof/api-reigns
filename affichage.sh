#!/bin/bash
###############################################
#Projetname  :  reigns
# Filename    :   affichage.sh
# Author      :   Shaofeng
# Description :   
# Version     :   1.0.0
###############################################


# Définir la couleur
RED='\E[1;31m'        # Rouge
GREEN='\E[1;32m'      # Vert
YELOW='\E[1;33m'      # Orange
BLUE='\E[1;34m'       # Bleu
PINK='\E[1;35m'       # Pink
SHAN='\E[33;5m'       #Orange Worning
RES='\E[0m'           # Clear


function affichage_etat()
{
    echo
    printf "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" ;
    echo
    echo -e "${SHAN}\t\t\t\t\t\tAnnee ${RES}"  $annee
    echo
    echo
    echo -e "\t${GREEN} Satisfaction : $Satisfaction${RES} \t   ${YELOW} Armee : $Armee${RES}\t\t${BLUE} Clerge : $Clerge ${RES}    \t${PINK} Richesse : $Richesse${RES}"
    printf "\t\t___\t\t\t___\t\t\t___\t\t\t___" ;
    echo
    for i in {1..10}
    do
    if [ $i -lt `expr 10 - $Satisfaction / 10` ] ;
    then
        printf "\t\t| |" ;
    else
        printf "\t\t|${GREEN}*${RES}|" ;
    fi

    if [ $i -lt `expr 10 - $Armee / 10` ] ;
    then
        printf "\t\t\t| |" ;
    else
        printf "\t\t\t|${YELOW}*${RES}|" ;
    fi

    if [ $i -lt `expr 10 - $Clerge / 10` ] ;
    then
        printf "\t\t\t| |" ;
    else
        printf "\t\t\t|${BLUE}*${RES}|" ;
    fi

    if [ $i -lt `expr 10 - $Richesse / 10` ] ;
    then
        printf "\t\t\t| |" ;
    else
        printf "\t\t\t|${PINK}*${RES}|" ;
    fi
    echo
    done
    printf "\t\t===\t\t\t===\t\t\t===\t\t\t===" ;
    echo
    echo
    printf "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" ;
    echo
    echo
}

function affichage_element()
{
    echo
    for i in {1..100}
    do
        printf "\r-" 
        for j in `seq $i`
        do
            printf "-" 
        done
        printf ">>>process %3d %%" $i
        sleep 0.01
    done
    echo
    printf "${SHAN}\t Nouvel évènement : ${RES}  ${Element_description[$1]}  "
    echo
    printf "============================================================================================================="
    echo
    echo
    printf "\t${PINK}${Element_choix_yes[$1]}${RES}"
    echo
    printf "\t\t\t<<-----------------------\\"
    echo
    printf "\t\t\t\t\tYES\t  \\ \t   NO"
    echo
    printf "\t\t\t\t\t\t   \\----------------------->>"
    echo
    printf "\t\t\t\t\t\t    ${GREEN}${Element_choix_no[$1]}${RES}"
    echo
    echo
    printf "============================================================================================================="
    echo
    echo
}

function comparer_changement_etat()
{
    echo
    printf "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" ;
    echo
    echo -e "${SHAN}\t\t\t\t\t\tAnnee ${RES}"  ${PINK}${Element_annee_yes[$1]}${RES}    $annee    ${GREEN}${Element_annee_no[$1]}${RES}
    echo
    echo -e "\t\t${PINK}YES :\t${Element_satisfaction_yes[$1]} \t\t    ${Element_armee_yes[$1]}    \t\t  ${Element_clerge_yes[$1]} \t\t\t    ${Element_richesse_yes[$1]}${RES} \t\t"
    echo -e "\t Satisfaction : $Satisfaction \t    Armee : $Armee\t\t Clerge : $Clerge     \t Richesse : $Richesse"
    echo -e "\t\t${GREEN} No :\t${Element_satisfaction_no[$1]} \t\t    ${Element_armee_no[$1]}    \t\t  ${Element_clerge_no[$1]} \t\t\t    ${Element_richesse_no[$1]}${RES} \t\t"
    echo
    printf "\t   ___  ___  ___\t   ___  ___  ___\t   ___  ___  ___\t   ___  ___  ___" ;
    echo
    for i in {1..10}
    do
    #yes Satisfaction no
    if [ $i -lt `expr 10 - $Satisfaction / 10 - $((0 ${Element_satisfaction_yes[$1]})) / 10` ] ;
    then
        printf "\t   | |" ;
    else
        printf "\t   |${PINK}*${RES}|" ;
    fi
    if [ $i -lt `expr 10 - $Satisfaction / 10` ] ;
    then
        printf "  | |  " ;
    else
        printf "  |*|  " ;
    fi
    if [ $i -lt `expr 10 -  $Satisfaction / 10 - $((0 ${Element_satisfaction_no[$1]})) / 10` ] ;
    then
        printf "| |   " ;
    else
        printf "|${GREEN}*${RES}|   " ;
    fi
    #yes Armee no
        if [ $i -lt `expr 10 - $Armee / 10 - $((0 ${Element_armee_yes[$1]})) / 10` ] ;
    then
        printf "\t   | |" ;
    else
        printf "\t   |${PINK}*${RES}|" ;
    fi
    if [ $i -lt `expr 10 - $Armee / 10` ] ;
    then
        printf "  | |  " ;
    else
        printf "  |*|  " ;
    fi
    if [ $i -lt `expr 10 - $Armee / 10 - $((0 ${Element_armee_no[$1]})) / 10` ] ;
    then
        printf "| |   " ;
    else
        printf "|${GREEN}*${RES}|   " ;
    fi
    #yes Armee no
        if [ $i -lt `expr 10 - $Clerge / 10 - $((0 ${Element_clerge_yes[$1]})) / 10` ] ;
    then
        printf "\t   | |" ;
    else
        printf "\t   |${PINK}*${RES}|" ;
    fi
    if [ $i -lt `expr 10 - $Clerge / 10` ] ;
    then
        printf "  | |  " ;
    else
        printf "  |*|  " ;
    fi
    if [ $i -lt `expr 10 - $Clerge / 10 - $((0 ${Element_clerge_no[$1]})) / 10` ] ;
    then
        printf "| |   " ;
    else
        printf "|${GREEN}*${RES}|   " ;
    fi
    #yes Armee no
        if [ $i -lt `expr 10 - $Richesse / 10 - $((0 ${Element_richesse_yes[$1]})) / 10` ] ;
    then
        printf "\t   | |" ;
    else
        printf "\t   |${PINK}*${RES}|" ;
    fi
    if [ $i -lt `expr 10 - $Richesse / 10` ] ;
    then
        printf "  | |  " ;
    else
        printf "  |*|  " ;
    fi
    if [ $i -lt `expr 10 - $Richesse / 10 - $((0 ${Element_richesse_no[$1]})) / 10` ] ;
    then
        printf "| |   " ;
    else
        printf "|${GREEN}*${RES}|   " ;
    fi
    echo
    done
    printf "\t   ===  ===  ===\t   ===  ===  ===\t   ===  ===  ===\t   ===  ===  ===" ;
    echo
    echo
    printf "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\" ;
    echo
    echo
}

function progress {
for i in {1..100}
do
    printf "\r-" 
    for j in `seq $i`
    do
        printf "-" 
    done
    printf ">>>process %3d %%" $i
    sleep 0.01
done
}

#echo -e affichage
#echo -e "${SHAN} this is yelow flashing warning ${RES}"  
#echo -e "${GREEN} this is green color ${RES}"
#echo -e "${YELOW} this is red yelow ${RES}"
#echo -e "${BLUE} this is blue color ${RES}"
#echo -e "${PINK} this is pink color ${RES}"

#affichage_etat

#affichage_element 0
#comparer_changement_etat 0
#affichage_element 1
#comparer_changement_etat 1
