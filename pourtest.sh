source affichage.sh
source  reigns.sh

Element_description[2]="Votre fille veut épouser un paysan, la bannir du royaume ?"
Element_choix_yes[2]="Cachez au moins un petit peu votre suffisance démesuré à l'égard des paysans !"
Element_annee_yes[2]=+6
Element_satisfaction_yes[2]=+0
Element_armee_yes[2]=-20
Element_clerge_yes[2]=+0
Element_richesse_yes[2]=+0
Element_choix_no[2]="Le peuple rêve de ce mariage et envie le paysan. Mais, ce n'est pas lui qui va renflouer les caisses du royaume !"
Element_annee_no[2]=+3
Element_satisfaction_no[2]=+10
Element_armee_no[2]=+10
Element_clerge_no[2]=-10
Element_richesse_no[2]=-25

Element_description[3]="Une épidémie se répand, garder les herbes médicinales pour vous ?"
Element_choix_yes[3]="La moitié de votre peuple est mort, le clergé croit à l'oeuvre de Dieu"
Element_annee_yes[3]=+7
Element_satisfaction_yes[3]=+0
Element_armee_yes[3]=-30
Element_clerge_yes[3]=+0
Element_richesse_yes[3]=-10
Element_choix_no[3]="C'est cher de garder des gens en vie, mais vous avez sauver le peuple, il vous surnomme \"le sauveur\" "
Element_annee_no[3]=+5
Element_satisfaction_no[3]=-10
Element_armee_no[3]=+20
Element_clerge_no[3]=+0
Element_richesse_no[3]=+0

Element_description[4]="Vous avez prêté de l'argent au duc et il ne vous l'a pas encore rendu. Aller lui réclamer l'argent ?"
Element_choix_yes[4]="Vous avez récupéré votre argent, mais vous vous êtes mis le Duc à dos."
Element_annee_yes[4]=+3
Element_satisfaction_yes[4]=+10
Element_armee_yes[4]=-10
Element_clerge_yes[4]=+0
Element_richesse_yes[4]=+0
Element_choix_no[4]="Certes, vous avez épargné la fierté du Duc, mais vous n'avez pas récupéré votre argent."
Element_annee_no[4]=+6
Element_satisfaction_no[4]=-10
Element_armee_no[4]=+5
Element_clerge_no[4]=+0
Element_richesse_no[4]=+0

game_begin
element=0

while [ $Satisfaction -le 100 ] && [ $Satisfaction -ge 0 ] && [ $Armee -le 100 ] && [ $Armee -ge 0 ] && [ $Clerge -le 100 ] && [ $Clerge -ge 0 ] && [ $Richesse -le 100 ] && [ $Richesse -ge 0 ]
do
    affichage_etat
    affichage_element $element
    comparer_changement_etat $element
    echo "Votre choix [yes|no] : "
    read choix
    while [ $choix != 'yes' ] && [ $choix != 'no' ]
    do
        echo "Votre choix [yes|no] : "
        read choix
    done
    if [ $choix = 'yes' ] 
    then
        change_etat_yes $element
    elif [ $choix = 'no' ] 
    then 
        change_etat_no $element
    fi
    element=$(( $RANDOM %  5 ))
done


affichage_etat
finish