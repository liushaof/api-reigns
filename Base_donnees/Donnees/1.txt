La fille du roi d'un pays voisin souhaite qu'un de vos fils l'épouse et habite dans son pays. Accepter ? 
+10 +5 0 0
0 -5 -5 0
Vous laissez partir votre fils, mais la dot de la fille du roi a permis de remplir un peu les caisses de l'Etat.
Le peuple n'aime pas le prince et n'est pas content de devoir le supporter plus longtemps. Des révoltes sont organisée et l'armée doit intervenir. 
