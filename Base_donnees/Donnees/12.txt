Noël approche et le peuple a faim. Organiser un grand festin gratuit ?
-15 +15 0 0
0 -15 -5 0
Certes nourrir un peuple entier coûte cher, mais tout le monde vous adore maintenant ; vous avez redoré votre image.
Le bruit court que le roi est avare... Personne n'est satisfait de votre décision, et l'armée doit réprimer quelques manifestations.

