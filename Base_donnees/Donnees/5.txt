Beaucoup de paysans n'ont plus assez d'argent pour se nourrir et souffrent de la famine. Instaurer un revenu universel ?
-20 +20 0 0
0 -20 0 0
Grâce à cette réforme, vous avez sauvé de nombreuses vies et le peuple vous adore. En contrepartie, les caisses de l'Etat se sont allégées.
La famine a entraîné beaucoup de morts.
