#!/bin/bash
###############################################
# Filename    :   reigns.sh
# Author      :   Shaofeng
# Description :   
# Version     :   1.0.0
###############################################

# valeur initiale
source affichage.sh

annee=1

Satisfaction=50
Armee=50
Clerge=50
Richesse=50

element=0

Element_description[0]="First element : Le nouveau roi intronisé, Putian fait la fête ensemble !"
Element_choix_yes[0]="YES"
Element_annee_yes[0]=+8
Element_satisfaction_yes[0]=+20
Element_armee_yes[0]=+20
Element_clerge_yes[0]=+20
Element_richesse_yes[0]=+20
Element_choix_no[0]="NO"
Element_annee_no[0]=+6
Element_satisfaction_no[0]=-20
Element_armee_no[0]=-20
Element_clerge_no[0]=-20
Element_richesse_no[0]=-20



Element_description[1]="Des ennemis étrangers arrivent !"
Element_choix_yes[1]="Envoyez des troupes pour résister et défendre votre patrie"
Element_annee_yes[1]=+15
Element_satisfaction_yes[1]=-10
Element_armee_yes[1]=-20
Element_clerge_yes[1]=+25
Element_richesse_yes[1]=+0
Element_choix_no[1]="Négocier un règlement"
Element_annee_no[1]=+5
Element_satisfaction_no[1]=+10
Element_armee_no[1]=+10
Element_clerge_no[1]=-10
Element_richesse_no[1]=-25

function game_begin()
{
    clear
    for i in {1..100}
    do
        printf "\r-" 
        for j in `seq $i`
        do
            printf "-" 
        done
        printf ">>>process %3d %%" $i
        sleep 0.01
    done
    cat ./gamebegin
    for i in {1..100}
    do
        printf "\r-" 
        for j in `seq $i`
        do
            printf "-" 
        done
        printf ">>>process %3d %%" $i
        sleep 0.02
    done
    echo
    echo 
    echo -e "${SHAN}\t\t\t\t\t\tEnter to start ~  ${RES}"
    read choix
}


function finish()
{
    cat ./gameover
}


function change_etat_yes()
{
    annee=$(($annee + 1))
    Satisfaction=$(($Satisfaction + ${Element_satisfaction_yes[$1]}))
    Armee=$(($Armee + ${Element_armee_yes[$1]}))
    Clerge=$(($Clerge + ${Element_clerge_yes[$1]}))
    Richesse=$(($Richesse + ${Element_richesse_yes[$1]}))
    echo "Yes"
}
function change_etat_no()
{
    annee=$(($annee + 1))
    Satisfaction=$(($Satisfaction + ${Element_satisfaction_no[$1]})) 
    Armee=$(($Armee + ${Element_armee_no[$1]})) 
    Clerge=$(($Clerge + ${Element_clerge_no[$1]}))
    Richesse=$(($Richesse + ${Element_richesse_no[$1]}))
    echo "No"
}

function checketat(){
if [ $Satisfaction -gt 100 ]; then 
Satisfaction=100
fi
if [ $Armee -gt 100 ]; then 
Armee=100
fi
if [ $Clerge -gt 100 ]; then 
Clerge=100
fi
if [ $Richesse -gt 100 ]; then 
Richesse=100
fi
}
