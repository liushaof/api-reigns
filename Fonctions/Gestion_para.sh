#!/bin/bash

tmp=$(sed '1!d' ../Base_donnees/Evol_jeu/Parametres.txt)
tmp=$(($tmp + $1))
sed -i "1c $tmp" ../Base_donnees/Evol_jeu/Parametres.txt

tmp=$(sed '2!d' ../Base_donnees/Evol_jeu/Parametres.txt)
tmp=$(($tmp + $2))
sed -i "2c $tmp" ../Base_donnees/Evol_jeu/Parametres.txt

tmp=$(sed '3!d' ../Base_donnees/Evol_jeu/Parametres.txt)
tmp=$(($tmp + $3))
sed -i "3c $tmp" ../Base_donnees/Evol_jeu/Parametres.txt

tmp=$(sed '4!d' ../Base_donnees/Evol_jeu/Parametres.txt)
tmp=$(($tmp + $4))
sed -i "4c $tmp" ../Base_donnees/Evol_jeu/Parametres.txt
